import { TestoPage } from './app.po';

describe('testo App', () => {
  let page: TestoPage;

  beforeEach(() => {
    page = new TestoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
