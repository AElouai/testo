import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateComparisonComponent } from './date-comparison.component';

describe('DateComparisonComponent', () => {
  let component: DateComparisonComponent;
  let fixture: ComponentFixture<DateComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
