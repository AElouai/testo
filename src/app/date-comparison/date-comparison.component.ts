import { Component, OnInit } from '@angular/core';
import {isUndefined} from "util";

@Component({
  selector: 'app-date-comparison',
  templateUrl: './date-comparison.component.html',
  styleUrls: ['./date-comparison.component.css']
})
export class DateComparisonComponent implements OnInit {

  start: Date ;
  end: Date ;
  result: Number  = 0;

  constructor() { }

  ngOnInit() {
  }

  change() {
    console.log('change' , this.start !== undefined && this.end !== undefined);
    if( this.start !== undefined && this.end !== undefined ) {
      this.result = Math.abs((new Date(this.start).getTime() - new Date(this.end).getTime()) / (1000 * 60 * 60 * 24 )) ;
    }
  }
}
